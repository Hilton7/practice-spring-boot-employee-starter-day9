# **What did we learn today? What activites did you do? What scenes have impressed you**
Today, I mainly learned the concept of cloud native and DTO, of which DTO impressed me the most.
# **Pleas use one word to express your feelings about today's class.**
useful.
# **What do you think about this? What was the most meaningful aspect of this activity?**
What I learned today is also more related to the actual development scenario, and the most meaningful activity is the speech I participated in.
# **Where do you most want to apply what you have learned today? What changes will you make?**
In the actual development scenario, I will add the DTO I learned today, and the DTO completes the data extraction, allowing me to focus more on the object data required by the business.