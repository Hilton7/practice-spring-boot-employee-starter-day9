package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.CompanyService;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
class CompanyControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        companyRepository.deleteAll();
        employeeRepository.deleteAll();
    }

    @Test
    void should_update_company_name() throws Exception {
        Company previousCompany = new Company(null, "abc");
        companyRepository.save(previousCompany);

        Company companyUpdateRequest = new Company(previousCompany.getId(), "xyz");
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedEmployeeJson = objectMapper.writeValueAsString(companyUpdateRequest);
        mockMvc.perform(put("/companies/{id}", previousCompany.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployeeJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Optional<CompanyResponse> optionalCompany = Optional.ofNullable(companyService.findById(previousCompany.getId()));
        assertTrue(optionalCompany.isPresent());
        CompanyResponse updatedCompany = optionalCompany.get();
        Assertions.assertEquals(previousCompany.getId(), updatedCompany.getId());
        Assertions.assertEquals(companyUpdateRequest.getName(), updatedCompany.getName());
    }

    @Test
    void should_delete_company_name() throws Exception {
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setName("abc");
        Company saveCompany = companyRepository.save(CompanyMapper.toEntity(companyRequest));
        EmployeeRequest employeeRequest = getEmployee(saveCompany);
        Employee saveEmployee = employeeRepository.save(EmployeeMapper.toEntity(employeeRequest));
        mockMvc.perform(delete("/companies/{id}", saveCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));
        assertFalse(companyRepository.findById(saveCompany.getId()).isPresent());
        assertFalse(employeeRepository.findById(saveEmployee.getId()).isPresent());
    }

    @Test
    void should_create_employee() throws Exception {
        CompanyRequest company = getCompany1();

        ObjectMapper objectMapper = new ObjectMapper();
        String companyRequest = objectMapper.writeValueAsString(company);
        mockMvc.perform(post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()));
    }

    @Test
    void should_find_companies() throws Exception {
        CompanyRequest company = getCompany1();
        companyRepository.save(CompanyMapper.toEntity(company));

        mockMvc.perform(get("/companies"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(companyRepository.findAll().size()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company.getName()));
    }

    @Test
    void should_find_companies_by_page() throws Exception {
        CompanyRequest company1 = getCompany1();
        CompanyRequest company2 = getCompany2();
        CompanyRequest company3 = getCompany3();
        companyRepository.save(CompanyMapper.toEntity(company1));
        companyRepository.save(CompanyMapper.toEntity(company2));
        companyRepository.save(CompanyMapper.toEntity(company3));

        mockMvc.perform(get("/companies")
                        .param("page", "1")
                        .param("size", "2"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(companyService.findByPage(1, 2).size()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(company1.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name").value(company2.getName()))
        ;
    }

    @Test
    void should_find_company_by_id() throws Exception {
        CompanyRequest company = getCompany1();
        Company saveCompany = companyRepository.save(CompanyMapper.toEntity(company));
        EmployeeRequest employee = getEmployee(saveCompany);
        employeeRepository.save(EmployeeMapper.toEntity(employee));

        mockMvc.perform(get("/companies/{id}", saveCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(company.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.employeeCount").value(companyService.findById(saveCompany.getId()).getEmployeeCount()));
    }

    @Test
    void should_find_employees_by_companies() throws Exception {
        CompanyRequest company = getCompany1();
        Company saveCompany = companyRepository.save(CompanyMapper.toEntity(company));
        EmployeeRequest employee = getEmployee(saveCompany);
        employeeRepository.save(EmployeeMapper.toEntity(employee));

        mockMvc.perform(get("/companies/{companyId}/employees", saveCompany.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(employee.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(employee.getAge()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value(employee.getGender()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(employee.getSalary()));
    }

    private static EmployeeRequest getEmployee(Company company) {
        EmployeeRequest employeeRequest = new EmployeeRequest();
        employeeRequest.setName("zhangsan");
        employeeRequest.setAge(22);
        employeeRequest.setGender("Male");
        employeeRequest.setSalary(10000);
        employeeRequest.setCompanyId(company.getId());
        return employeeRequest;
    }


    private static CompanyRequest getCompany1() {
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setName("ABC");
        return companyRequest;
    }

    private static CompanyRequest getCompany2() {
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setName("DEF");
        return companyRequest;
    }

    private static CompanyRequest getCompany3() {
        CompanyRequest companyRequest = new CompanyRequest();
        companyRequest.setName("XYZ");
        return companyRequest;
    }
}