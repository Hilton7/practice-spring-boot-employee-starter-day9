package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import org.springframework.beans.BeanUtils;

import java.util.Objects;

public class CompanyMapper {
    public static Company toEntity(CompanyRequest companyRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest, company);
        return company;
    }

    public static CompanyResponse toResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company, companyResponse);
        if (Objects.isNull(company.getEmployees()))
            companyResponse.setEmployeeCount(0);
        else
            companyResponse.setEmployeeCount(company.getEmployees().size());
        return companyResponse;
    }
}
