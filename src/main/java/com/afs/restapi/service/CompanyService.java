package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.exception.ParamException;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {


    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;

    public CompanyService() {

    }


    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        if (page <= 0 || size <= 0) {
            throw new ParamException("page and size must be morn than 0");
        }
        List<Company> companies = this.findAll();
        List<Company> result;
        try {
            int startNum = (page - 1) * size;
            if (startNum >= companies.size()) {
                return companies.subList(companies.size(), companies.size());
            } else if (startNum + size >= companies.size()) {
                result = companies.subList(startNum, companies.size());
            } else {
                result = companies.subList(startNum, startNum + size);
            }
        } catch (Exception e) {
            return null;
        }
        return result;
    }

    public CompanyResponse findById(Long id) {
        Company company = companyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        List<Employee> employees = employeeRepository.findAll();
        employees = employees.stream().filter(employee -> employee.getCompanyId().equals(id)).collect(Collectors.toList());
        company.setEmployees(employees);
        return CompanyMapper.toResponse(company);
    }

    public CompanyResponse update(Long id, CompanyRequest companyRequest) {
        Company updateCompany = companyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        if (companyRequest.getName() == null || companyRequest.getName().trim().equals("")) {
            throw new ParamException("company name can not be empty");
        }
        updateCompany.setName(companyRequest.getName().trim());
        companyRepository.save(updateCompany);
        return CompanyMapper.toResponse(updateCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        return CompanyMapper.toResponse(companyRepository.save(CompanyMapper.toEntity(companyRequest)));
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        List<Employee> employees = companyRepository.findById(id).map(Company::getEmployees).get();
        return employees;
    }

    public void delete(Long id) {
        companyRepository.deleteById(id);
    }
}
