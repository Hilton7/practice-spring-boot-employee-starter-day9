package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.exception.ParamException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeService() {

    }

    public List<Employee> findAll() {
        return employeeRepository.findAll().stream()
                .collect(Collectors.toList());
    }

    public EmployeeResponse findById(Long id) {
        Employee employee = employeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
        return EmployeeMapper.toResponse(employee);
    }

    public EmployeeResponse update(Long id, EmployeeRequest employeeRequest) {
        Employee toBeUpdatedEmployee = employeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
        if (employeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(employeeRequest.getAge());
        }
        employeeRepository.save(toBeUpdatedEmployee);
        return EmployeeMapper.toResponse(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        List<Employee> employees = employeeRepository.findAll();
        return employees.stream().filter(employee -> employee.getGender().equalsIgnoreCase(gender)).collect(Collectors.toList());
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {

        return EmployeeMapper.toResponse(employeeRepository.save(EmployeeMapper.toEntity(employeeRequest)));
    }

    public List<Employee> findByPage(Integer page, Integer size) {
        if (page <= 0 || size <= 0) {
            throw new ParamException("page and size must be morn than 0");
        }
        List<Employee> employees = employeeRepository.findAll();
        List<Employee> result;
        try {
            int startNum = (page - 1) * size;
            if (startNum >= employees.size()) {
                return employees.subList(employees.size(), employees.size());
            } else if (startNum + size >= employees.size()) {
                result = employees.subList(startNum, employees.size());
            } else {
                result = employees.subList(startNum, startNum + size);
            }
        } catch (Exception e) {
            return null;
        }
        return result;
    }

    public void delete(Long id) {
        employeeRepository.deleteById(id);
    }
}
