package com.afs.restapi.exception;

public class ParamException extends RuntimeException {
    public ParamException(String message) {
        super(message);
    }
}
