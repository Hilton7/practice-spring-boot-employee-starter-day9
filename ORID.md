# **What did we learn today? What activites did you do? What scenes have impressed you**
Today, I learned the on-premises data layer of the three-tier development architecture, among which the easy operation data of JPA impressed me the most.
# **Pleas use one word to express your feelings about today's class.**
useful.
# **What do you think about this? What was the most meaningful aspect of this activity?**
What I have learned today is the content of ideas that will be often used in future development, and I am very impressed by the simplicity of JPA operation data.
# **Where do you most want to apply what you have learned today? What changes will you make?**
Today I learned the use of JPA and the idea of data manipulation layer separation, which gave my development thinking a new level of thinking.